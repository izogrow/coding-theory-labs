import bitarray as ba
import numpy as np


# a = np.array([1, 0, 0, 1])
# g = np.array([1, 0, 1, 1])
#
# c = np.array([0, 0, 0, 0, 0, 0, 0])
# for i in range(a.size):
#     if a[i] == 1:
#         c[i:i+4] = np.logical_xor(c[i:i+4], g)
# print(c)

a = [1, 2, 3]
print(a[:2])
