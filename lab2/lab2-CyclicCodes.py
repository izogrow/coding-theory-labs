import numpy as np
import bitarray as ba
import random
import pathlib

from CodeUtils import CodeUtils
cur_path = pathlib.Path()

class CyclicCodes:
    k = 4  #7   #4
    n = 7 #15  #7
    n_k = n - k
    t = 1 # 2  #1   # максимальная кратность ошибок для таблицы синдромов
    g = np.array([1, 0, 1, 1]) # np.array([1, 0, 0, 0, 1, 0, 1, 1, 1]) # np.array([1, 0, 1, 1])
    bone = {}


    def set_g(self, g):
        self.g = g

    def encode(self, a):
        c = np.array([0] * self.n)
        for i in range(a.size):
            if a[i] == 1:
                c[i:i + self.k] = np.logical_xor(c[i:i + self.k], self.g)
        return ba.bitarray(c.tolist())

    # def encode(self, bit_arr: ba.bitarray, is_reshaped: bool = False) -> ba.bitarray:
    #     """
    #     :param bit_arr: bitarray that length multiple of self.k
    #     :param is_reshaped: flag is it array shape - into ndarray (l,self.k), where l - some value greater 0
    #     :return: encoded array multiple of self.n
    #     """
    #     bit_list = CodeUtils.reshape_to_np_arr(bit_arr, self.k, is_reshaped)
    #     coded_arr = ba.bitarray()
    #     # encode
    #     for i in range(len(bit_list)):
    #         curr_np = np.asarray([False] * self.n)
    #         for j in range(self.k):
    #             if bit_list[i][j]:
    #                 xj_g = [False] * j
    #                 xj_g.extend(self.g.tolist())
    #                 xj_g.extend([False] * (self.n_k - j))
    #                 xj_g = np.asarray(xj_g)
    #                 curr_np = np.logical_xor(curr_np, xj_g)
    #         else:
    #             coded_arr.extend(curr_np)
    #
    #     return coded_arr

    def remainder(self, c):
        rem = c.copy()

        if isinstance(rem, ba.bitarray):
            rem = rem.tolist()

        for i in range(self.n - 1, self.n_k -1, -1):
            if rem[i] == 1:
                #   print("[",i - (self.n - self.k), ", ", i + 1, "]")
                #   print("rem ",rem[i - (self.n - self.k):i + 1])
                rem[i - self.n_k:i + 1] = np.logical_xor(rem[i - self.n_k:i + 1], self.g)
            #   print("here ", rem[i - (self.n - self.k):i + 1])
            #   print("temp rem", rem)

        return ba.bitarray(np.array(rem).tolist())

    def encody_sys(self, a):
        c_shifted = np.array([0, 0, 0, 0, 0, 0, 0])
        c_shifted[self.n - self.k:] = a
        r = self.remainder(c_shifted)
        print(r)
        c_shifted = np.logical_xor(c_shifted, r)
        return ba.bitarray(c_shifted.tolist())

    def get_syndrome_dict(self):
        syndrome_dict = {}

        for i in range(2 ** self.n):
            err = ba.bitarray(CodeUtils.int_to_str_bits(i, self.n))
            if CodeUtils.wt(err) <= self.t:
                #err_np = CodeUtils.get_num_from_bit(err)
                synd = ba.bitarray(self.remainder(err)[:self.n_k])
                str_synd = CodeUtils.ba_to_str_bits(synd)
                print(CodeUtils.wt(err), CodeUtils.ba_to_str_bits(err), str_synd)
                syndrome_dict[str_synd] = np.nonzero(list(err))[0]
        #print(f"len(syndrome_dict) {len(syndrome_dict)}")
        return syndrome_dict

    def decode_sys(self, bit_arr: ba.bitarray, make_table, is_fix_err: bool = True):
        """
        With correct.
        :param bit_arr:
        :param is_reshaped:
        :return:
        """
        bit_arr = CodeUtils.add_to_multiplicity_n(bit_arr, self.n)
        decoded = ba.bitarray()
        remain_d = None
        syndromes = None
        if is_fix_err:
            remain_d = self.remainder(bit_arr)
            syndromes = make_table()

        # print("len synd", len(syndromes))
        for i in range(len(bit_arr) // self.n):
            i_beg = i * self.n
            if is_fix_err:
                # print(self.n_k)
                key = CodeUtils.ba_to_str_bits(remain_d[i_beg:i_beg + self.n_k])
                index_err_bit = syndromes[key]
                if len(index_err_bit):
                    print(bit_arr)
                    for index_err in index_err_bit:
                        bit_arr[i_beg + index_err] = not bit_arr[i_beg + index_err]
                    else:
                        print(" " * (10 + i_beg + index_err_bit[0]) + "|" * (2))
                        print(bit_arr)
                        print(index_err_bit)
                        print(key)

                        print(f"{index_err_bit[0]} from {self.n}", )
                        print(f"error found. It index is {i_beg + index_err_bit[0]}", "\n")

            decoded.extend(bit_arr[i_beg + self.n_k:i_beg + self.n])

        return decoded

    def decode_file(self, file_in, file_out, make_table, is_fix_err: bool = True):
        """
        Have inner dependence from method encode_file
        Doesn't recomended to call it without calling the encode_file method.
        You can get back more bits what in really msg.

        Example
        You have n = 15, k=9
        and lenght of msg in bits is 136.
        To decode msg u need extend bitarry to 144 bit (144⋮ 9 144⋮ 8). And it also divide by 8 without remain.
        144 = 8 * 9 * 2
        And after decoding, u need delete byte at the end of msg

        And in other case u have msg with 144 bits lenght
        :param file_in:
        :param file_out:
        :param make_table: function is returned the special dict. See CyclicCode.make_table()
        :param is_fix_err:
        :return:
        """
        bit_a = ba.bitarray()
        bit_a.fromfile(file_in)

        print(f"endcode {len(bit_a)}, {bit_a}")
        bit_a = CodeUtils.add_to_multiplicity_n(bit_a, self.n, is_to_less=True)
        print(f"endcode {len(bit_a)}, {bit_a}")

        decode = self.decode_sys(bit_a, make_table, is_fix_err=is_fix_err)

        key = len(decode)
        true_len = self.bone.setdefault(key, None)
        if true_len:
            decode = decode[:true_len]
        # clear the memory
        del self.bone[key]

        print(f"decode  {len(decode)}, {decode}")
        # decode = CodeUtils.add_to_multiplicity_n(decode, 8, is_to_less=True)
        decode.tofile(file_out)

    def encode_file(self, file_in, file_out):
        """

        :param file_in:
        :param file_out:
        :return:
        """

        bit_a = ba.bitarray()
        bit_a.fromfile(file_in)
        len_bit_a = len(bit_a)

        print(f"our msg {len(bit_a)}, {bit_a}")
        bit_a = CodeUtils.add_to_multiplicity_n(bit_a, self.k)
        print(f"our msg {len(bit_a)}, {bit_a}")

        if len(bit_a) - len_bit_a >= 8:
            self.bone[len(bit_a)] = len_bit_a

        encode = self.encody_sys(bit_a)
        print(f"endcode {len(encode)}, {encode}")

        encode.tofile(file_out)

    def make_err_ba(self, bit_a: ba.bitarray):
        """

        :param bit_a: have lenght > 0
        :return:
        """
        rand_index = random.randint(0, len(bit_a) - 1)
        print(f"Create error at index {rand_index}")

        bit_a[rand_index] = not bit_a[rand_index]
        return bit_a

    def make_err_file(self, file_in, file_out, make_err_function):
        bit_a = ba.bitarray()
        bit_a.fromfile(file_in)

        bit_a = make_err_function(bit_a)
        bit_a.tofile(file_out)

def main():
    # a = np.array([0, 0, 1, 1])
    a = np.array([1, 0, 1, 0])
    cc = CyclicCodes()
    c = cc.encode(a)
    print("a = ", a)
    print("c = ", c)
    # c = np.array([0,0,0,0,0,1,1])
    rem = cc.remainder(c)
    print("rem =", rem)
    print("a =", a)
    encoded_a = cc.encody_sys(a)
    print("encoded_a = ", encoded_a)
    # a_encoded = cc.encode_sys(a)
    # print(a_encoded)

    syndromes = cc.get_syndrome_dict()
    print("syndromes", syndromes)

    print("\n\nTask 6 ")
    with open(cur_path / "input.txt", "rb") as file_in:
        with open(cur_path / "output.txt", "wb") as file_out:
            cc.encode_file(file_in, file_out)

        with open(cur_path / "output.txt", "rb") as file_out:
            with open(cur_path / "err.txt", "wb") as file_err:
                cc.make_err_file(file_out, file_err, cc.make_err_ba)

        with open(cur_path / "output.txt", "wb") as file_out:
            with open(cur_path / "err.txt", "rb") as file_err:
                cc.decode_file(file_err, file_out, cc.get_syndrome_dict)


if __name__ == '__main__':
    main()
