import numpy as np
import bitarray as ba
from random import randint

#1
#print(1)

H = np.array([[1, 1, 1, 0, 1, 0, 0], [1, 1, 0, 1, 0, 1, 0], [1, 0, 1, 1, 0, 0, 1]])
G = np.array([[1, 0, 0, 0, 1, 1, 1], [0, 1, 0, 0, 1, 1 ,0], [0, 0, 1, 0 ,1, 0, 1], [0, 0, 0, 1, 0, 1, 1]])
a = np.array([1, 0, 1, 0])

c = a.dot(G) % 2
print((H.dot(G.transpose()) % 2))
print("c =", c)

#2

b = H.dot(c.transpose()) % 2
print(b == 0)

e = [0, 0, 0, 0, 1, 0, 0]

c_out = (c + e) % 2
print("c' =", c_out)

b_out = H.dot(c_out.transpose()) % 2
print("b_out =", b_out)

#3
ba1 = ba.bitarray()
f = open("./in.txt", 'rb')
ba1.fromfile(f)
print(ba1)


#4
#ba1.length()
ba2 = ba.bitarray()
c_new = []
for i in range(0, len(ba1), 4):
    print(ba1[i:i+4])
    ai = ba1[i:i+4]
    ai_arr = []
    for x in ai:
        ai_arr.append(x)

    print(ai_arr)
    ai_arr = np.array(ai_arr)
    
    ci = (ai_arr.dot(G)) % 2
    print(ci)
    c_new.extend(ci.tolist())

print(c_new)
ba2 = ba.bitarray(c_new)
print(ba2)
print(len(ba1), " -> ", len(ba2))
with open("out.txt", "wb") as file:
    ba2.tofile(file)

#5

ba3 = ba.bitarray()
c_new = []
with open("out.txt", "rb") as file:
    ba3.fromfile(file)
    print(len(ba3))
    length = len(ba3) // 7 * 7
    print(length)
    for i in range(0, length, 7):
        print(ba3[i:i+7])
        ai = ba3[i:i+4]
        ai_arr = []
        for x in ai:
            ai_arr.append(x)

        print(ai_arr)
        
        c_new.extend(ai_arr)
    print(c_new)
    print(len(c_new))
    ba4 = ba.bitarray(c_new)
    with open("out2.txt", "wb") as file_out:
        ba4.tofile(file_out)
    

#6

ba4 = ba.bitarray()
with open("in.7z", "rb") as file:
    ba4.fromfile(file)
    for i in range(0, len(ba1), 4):
        print(ba1[i:i+4])
        ai = ba1[i:i+4]
        ai_arr = []
        for x in ai:
            ai_arr.append(x)
        
        print(ai_arr)
        ai_arr = np.array(ai_arr)
